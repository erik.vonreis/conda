# The conda class
#
# @summary Install conda environments.
#
# @example Basic usage
#   class {'::conda':
#       home      => '/var/opt/conda',
#       user      => 'cdsadmin',
#   }
#   conda::env {'cds-py39-20220316': }
#
# @param home [String]
#   The location where conda will be installed.  Environments will be put into $home/base/envs
#
# @param user [String]
#   The user to use when installing environments.  Defaults to 'root'.  If a non-root user is specified, then the '/root' home directory must be world readable (due to a bug in mamba).
#
# @param forge_url [String]
#   If specified, packages will be pulled from this condaforge mirror
#
# @param environs [Hash]
#   A list of conda environments to install
#
# @param default_env [String]
#   The name of the default environment to activate when users log in.  Defaults to an empty string, which means no environment will be activated.


class conda (
    String $home,
    String $user,
    String $forge_url,
    String $timer_ensure,
    Boolean $timer_enable,
    Optional[Boolean] $remove_missing = false,

    # graphical systems to target for script install
    Optional[Array[String]] $install_graphical = ["X11"],

    Optional[String] $default_env = '',
    Optional[Hash[String, Optional[Struct[ {Optional[ensure] => Enum['present', 'absent'],
                                            Optional[forge_url] => String,
                                            Optional[target] => String,
                                            Optional[update] => Boolean}]]]] $environs = undef,
) {
    $conda_install = "${home}/install"
    $conda_base = "${home}/base"

    file {'/etc/profile.d/conda.sh':
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0755',
        content => epp('conda/etc/profile.d/conda.sh', {
            conda_base  => $conda_base,
            default_env => $default_env,
        }),
    }

    if "X11" in $install_graphical {
      file {'/etc/X11/Xsession.d/99conda-activate':
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => epp('conda/etc/profile.d/conda.sh', {
          conda_base  => $conda_base,
          default_env => $default_env,
        }),
      }
    }

    $link_environs = $environs.filter|$items|{$items[1] and $items[1][target]}
    $real_environs = $environs.filter|$items|{!($items[1] and $items[1][target])}

    # handle real environments (not symlinks)

    if $real_environs {
      $install_envs = $real_environs.filter|$items|{(!$items[1]) or $items[1][ensure] != 'absent'}.map|$items|{$items[0]}
      $remove_envs = $real_environs.filter|$items|{$items[1] and $items[1][ensure] == 'absent'}.map|$items|{$items[0]}
      $update_envs = $real_environs.filter|$items|{$items[1] and $items[1][update]}.map|$items|{$items[0]}
    }
    else {
      $install_envs = []
      $update_envs = []
      $remove_envs = []
    }

    $systemd_dir = '/etc/systemd/system'

    file { ['/etc/systemd', $systemd_dir]:
      ensure => 'directory',
    }

    file { "${systemd_dir}/conda.timer":
      ensure => 'file',
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      source => 'puppet:///modules/conda/systemd/conda.timer',
    }

    file { "${systemd_dir}/conda.service":
      ensure  => 'file',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => epp('conda/systemd/conda.service',
        {
          install_envs => $install_envs,
          update_envs  => $update_envs,
          remove_envs  => $remove_envs,
          user         => $user,
        }),
    }

    file { '/usr/bin/update_conda':
      ensure  => 'file',
      owner   => 'root',
      group   => 'root',
      mode    => '0755',
      content => epp('conda/bin/update_conda', {
        remove_missing     => $remove_missing,
        conda_base_path    => $conda_base,
        conda_install_path => $conda_install,
        forge_url          => $forge_url,
        }),
      }

    exec {'conda daemon reload':
      command     => '/usr/bin/systemctl daemon-reload',
      refreshonly => true,
      subscribe   => [File["${systemd_dir}/conda.timer"], File["${systemd_dir}/conda.service"]],
    }

    service { 'conda.timer':
      ensure => $timer_ensure,
      enable => $timer_enable,
    }

    # handle symlink environs
    $link_environs.each|$items| {
      file { "${conda_base}/envs/${items[0]}":
        ensure => link,
        target => $items[1][target],
      }
    }
}
