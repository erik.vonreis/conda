# conda

Welcome to your new module. A short overview of the generated parts can be found
in the [PDK documentation][1].

The README template below provides a starting point with details about what
information to include in your README.

## Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with conda](#setup)
    * [What conda affects](#what-conda-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with conda](#beginning-with-conda)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Limitations - OS compatibility, etc.](#limitations)
1. [Development - Guide for contributing to the module](#development)

## Description

This module installs conda environments into a system-wide location from conda-forge or a local mirror.

## Setup

### What conda affects

A new conda setup, including any requested environments, will be installed in the location specified.

A new script will be put into /etc/profile.d/conda.sh to add conda to all user's path.  This script will optionally activate a specific environment for all non-system users.

### Setup Requirements **OPTIONAL**

If using a local conda-forge mirror, it must already be available.  yaml files describing each environment must also be made available at either a URL or local file.

Instructions for setting up a mirror are beyond the scope of this module, but can be deduced from https://git.ligo.org/cds-admin/teststand-puppet/-/tree/production/modules/condamirror/manifests

### Beginning with conda

The very basic steps needed for a user to get the module up and running. This
can include setup steps, if necessary, or it can be an example of the most basic
use of the module.

## Usage

To install a single environment named `cds-py39-20220316` in `/var/opt/conda`:

```
    class {'conda':
        home     => '/var/opt/conda',
    }
    conda::env {'cds-py39-20220316': }
```

To install as a non-root user using a conda-forge mirror, and providing a `cds` alias for the environment:

```
    class {'conda':
        home      => '/var/opt/conda',
        user      => 'cdsadmin',
        forge_url => 'http://puppet1/conda/environments/linux-64',
    }
    conda::env {'cds-py39-20220316': }
    conda::env {'cds': target => 'cds-py39-20220316', }
```

Specify the environments using hiera, with an alias named `cds` for the environment that will be activated for all users:

```
conda::environs:
  'cds':
    target: 'cds-py39-20220316'
  'cds-py39-20220314':
    ensure => 'absent'
  'cds-py39-20220315':
  'cds-py39-20220316':
```
```
    class {'conda':
        home        => '/var/opt/conda',
        user        => 'cdsadmin',
        forge_url   => 'http://puppet1/conda/environments/linux-64',
        default_env => 'cds',
    }
```

## Limitations

This module has only been tested on RHEL7 and RHEL8 systems.  It may work on others.
